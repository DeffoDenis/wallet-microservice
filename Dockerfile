FROM openjdk:8-jdk-alpine
RUN mkdir database-h2
ADD database-h2 /database-h2
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]