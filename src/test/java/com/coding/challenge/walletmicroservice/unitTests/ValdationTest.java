package com.coding.challenge.walletmicroservice.unitTests;

import com.coding.challenge.walletmicroservice.controller.request.SaveUpdateRequest;
import com.coding.challenge.walletmicroservice.controller.response.ErrorCode;
import com.coding.challenge.walletmicroservice.exception.ValidationException;
import com.coding.challenge.walletmicroservice.utils.TransactionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ValdationTest {

    public static final List<Enum> errorFullMessages = new ArrayList<>();
    public static final List<Enum> errorFullMessagesErr_01 = new ArrayList<>();
    public static final List<Enum> errorFullMessagesErr_02 = new ArrayList<>();
    public static final List<Enum> errorFullMessagesErr_03 = new ArrayList<>();

    @BeforeAll
    public static void init(){
        errorFullMessages.add(ErrorCode.ERR_01);
        errorFullMessages.add(ErrorCode.ERR_02);
        errorFullMessages.add(ErrorCode.ERR_03);

        errorFullMessagesErr_01.add(ErrorCode.ERR_01);
        errorFullMessagesErr_02.add(ErrorCode.ERR_02);
        errorFullMessagesErr_03.add(ErrorCode.ERR_03);
    }

    @Test
    public void testTrimAndLowerCaseMethod(){
        Assertions.assertEquals("denis", TransactionUtils.trimLowerCaseString(" denis   "),"testTrimAndLowerCaseMethod");
        Assertions.assertEquals("deffo", TransactionUtils.trimLowerCaseString(" deffo"),"testTrimAndLowerCaseMethod");
        Assertions.assertEquals("def fo", TransactionUtils.trimLowerCaseString(" def fo"),"testTrimAndLowerCaseMethod");
        Assertions.assertEquals("deffo", TransactionUtils.trimLowerCaseString(" DEFFO"),"testTrimAndLowerCaseMethod");
        Assertions.assertNotEquals("deffo", TransactionUtils.trimLowerCaseString(" DEF FO"),"testTrimAndLowerCaseMethod");
        Assertions.assertThrows(NullPointerException.class, () -> TransactionUtils.trimLowerCaseString(null));
    }

    @Test
    public void testCheckEmptyListMethod(){
        Assertions.assertFalse(TransactionUtils.checkEmptyList(Arrays.asList("test")),"testCheckEmptyListMethod");
        Assertions.assertTrue(TransactionUtils.checkEmptyList(new ArrayList()),"testCheckEmptyListMethod");
        Assertions.assertTrue(TransactionUtils.checkEmptyList(null),"testCheckEmptyListMethod");
    }

    @Test
    public void testCheckEmptyStringMethod(){
        Assertions.assertFalse(TransactionUtils.checkEmptyString("test"),"testCheckEmptyStringMethod");
        Assertions.assertTrue(TransactionUtils.checkEmptyString(""),"testCheckEmptyStringMethod");
        Assertions.assertTrue(TransactionUtils.checkEmptyString(null),"testCheckEmptyStringMethod");
        Assertions.assertTrue(TransactionUtils.checkEmptyString(new String()),"testCheckEmptyStringMethod");
    }

    @Test
    public void testValidateSaveUpdateRequest() throws Exception {

        SaveUpdateRequest saveUpdateRequest = new SaveUpdateRequest();
        ValidationException validationException = null;

        Assertions.assertThrows(ValidationException.class , () ->TransactionUtils.validateSaveUpdateRequest(saveUpdateRequest),"testValidateSaveUpdateRequest");
        validationException = Assertions.assertThrows(ValidationException.class , () ->TransactionUtils.validateSaveUpdateRequest(saveUpdateRequest),"testValidateSaveUpdateRequest");
        errorFullMessages.removeAll(validationException.getEnumList());
        Assertions.assertTrue(errorFullMessages.isEmpty());

        saveUpdateRequest.setPlayerName("Deffo");
        saveUpdateRequest.setAmount(new BigDecimal(20));
        validationException = Assertions.assertThrows(ValidationException.class , () ->TransactionUtils.validateSaveUpdateRequest(saveUpdateRequest),"testValidateSaveUpdateRequest");
        errorFullMessagesErr_01.removeAll(validationException.getEnumList());
        Assertions.assertTrue(errorFullMessagesErr_01.isEmpty());

        saveUpdateRequest.setTransactionLogId("1234");
        saveUpdateRequest.setAmount(new BigDecimal(20));
        saveUpdateRequest.setPlayerName(null);
        validationException = Assertions.assertThrows(ValidationException.class , () ->TransactionUtils.validateSaveUpdateRequest(saveUpdateRequest),"testValidateSaveUpdateRequest");
        errorFullMessagesErr_02.removeAll(validationException.getEnumList());
        Assertions.assertTrue(errorFullMessagesErr_02.isEmpty());

        saveUpdateRequest.setTransactionLogId("1234");
        saveUpdateRequest.setAmount(null);
        saveUpdateRequest.setPlayerName("Denis");
        validationException = Assertions.assertThrows(ValidationException.class , () ->TransactionUtils.validateSaveUpdateRequest(saveUpdateRequest),"testValidateSaveUpdateRequest");
        errorFullMessagesErr_03.removeAll(validationException.getEnumList());
        Assertions.assertTrue(errorFullMessagesErr_03.isEmpty());

        saveUpdateRequest.setAmount(new BigDecimal(30));
        Assertions.assertDoesNotThrow(() -> TransactionUtils.validateSaveUpdateRequest(saveUpdateRequest),"testValidateSaveUpdateRequest");
    }

    @Test
    public void testValidateHistoryRequestMethod(){
        Assertions.assertDoesNotThrow(() -> TransactionUtils.validateHistoryRequest("deffo"),"testValidateHistoryRequestMethod");
        Assertions.assertThrows(ValidationException.class,() -> TransactionUtils.validateHistoryRequest(""),"testValidateHistoryRequestMethod");
        Assertions.assertThrows(ValidationException.class,() -> TransactionUtils.validateHistoryRequest(null),"testValidateHistoryRequestMethod");
        ValidationException validationException = Assertions.assertThrows(ValidationException.class,() -> TransactionUtils.validateHistoryRequest(null),"testValidateHistoryRequestMethod");
        Assertions.assertFalse(validationException.getEnumList().isEmpty());
        Assertions.assertEquals(ErrorCode.ERR_02,validationException.getEnumList().get(0));
    }
}
