package com.coding.challenge.walletmicroservice.integrationTests;

import com.coding.challenge.walletmicroservice.controller.request.SaveUpdateRequest;
import com.coding.challenge.walletmicroservice.controller.response.ErrorCode;
import com.coding.challenge.walletmicroservice.exception.CustomException;
import com.coding.challenge.walletmicroservice.model.TransactionLog;
import com.coding.challenge.walletmicroservice.model.TransactionOperation;
import com.coding.challenge.walletmicroservice.model.dto.TransactionDTO;
import com.coding.challenge.walletmicroservice.model.dto.TransactionLogDTO;
import com.coding.challenge.walletmicroservice.model.mapper.TransactionLogMapper;
import com.coding.challenge.walletmicroservice.model.mapper.TransactionMapper;
import com.coding.challenge.walletmicroservice.service.TransactionLogService;
import com.coding.challenge.walletmicroservice.service.TransactionService;
import com.coding.challenge.walletmicroservice.service.implementation.TransactionServiceImpl;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;

import java.math.BigDecimal;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Sql(scripts = "classpath:insert.sql")
@Import({TransactionServiceImpl.class, TransactionMapper.class, TransactionLogMapper.class})
@TestPropertySource(
        locations = "classpath:application-test.properties")
public class ServiceTest {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private TransactionLogService transactionLogService;

    @BeforeAll
    public static void init() throws Exception {
    }

    @Test
    @Order(1)
    public void testFindTransactionById() throws Exception {

        Assertions.assertEquals("denis",this.transactionService.findTransactionById(1L).getPlayerName(),"testFindTransactionById");
        Assertions.assertThrows(NullPointerException.class, () -> this.transactionService.findTransactionById(2L).getPlayerName(),"testFindTransactionById");
        Assertions.assertNull(this.transactionService.findTransactionById(2L),"testFindTransactionById");
    }

    @Test
    @Order(2)
    public void testFindTransactionByPlayerName() throws Exception {

        Assertions.assertEquals("denis",this.transactionService.findTransactionByPlayerName("denis").getPlayerName(),"testFindTransactionById");
        Assertions.assertThrows(NullPointerException.class, () -> this.transactionService.findTransactionByPlayerName("Deffo").getPlayerName(),"testFindTransactionById");
        Assertions.assertNull(this.transactionService.findTransactionByPlayerName("Deffo"),"testFindTransactionById");
    }

    @Test
    @Order(3)
    public void testFindTransactionLogById() throws Exception {

        Assertions.assertEquals("1",this.transactionLogService.findTransactionLogById("1").getTransactionLogId(),"testFindTransactionById");
        Assertions.assertThrows(NullPointerException.class, () -> this.transactionLogService.findTransactionLogById("2").getPlayerName(),"testFindTransactionById");
    }

    @Test
    @Order(4)
    public void testSaveTransactionMethodCredit() throws Exception {
        SaveUpdateRequest saveUpdateRequest = new SaveUpdateRequest();
        saveUpdateRequest.setTransactionLogId("1");
        saveUpdateRequest.setAmount(new BigDecimal("3000"));
        saveUpdateRequest.setPlayerName("denis");

        Assertions.assertThrows(CustomException.class, () -> this.transactionLogService.saveTransactionLog(saveUpdateRequest,
                TransactionOperation.CREDIT));
        saveUpdateRequest.setTransactionLogId("2");
        Assertions.assertDoesNotThrow(() -> this.transactionLogService.saveTransactionLog(saveUpdateRequest,
                TransactionOperation.CREDIT));
        TransactionLogDTO transactionLogDTO = this.transactionLogService.findTransactionLogById("2");
        TransactionDTO transactionDTO = this.transactionService.findTransactionByPlayerName("denis");
        Assertions.assertNotNull(transactionLogDTO);
        Assertions.assertNotNull(transactionDTO);
        Assertions.assertEquals(TransactionOperation.CREDIT,transactionLogDTO.getOperation());
        Assertions.assertEquals(3000,transactionLogDTO.getAmount().intValue());
        Assertions.assertEquals(6000,transactionDTO.getAmount().intValue());
        Assertions.assertNotNull(transactionDTO.getBaseModel().getUpdateDate());

    }

    @Test
    @Order(5)
    public void testSaveTransactionMethodDebit() throws Exception {
        SaveUpdateRequest saveUpdateRequest = new SaveUpdateRequest();
        CustomException customException = null;
        saveUpdateRequest.setTransactionLogId("3");
        saveUpdateRequest.setPlayerName("deffo");

        Assertions.assertThrows(CustomException.class, () -> this.transactionLogService.saveTransactionLog(saveUpdateRequest,
                TransactionOperation.DEBIT));
        customException = Assertions.assertThrows(CustomException.class, () -> this.transactionLogService.saveTransactionLog(saveUpdateRequest,
                TransactionOperation.DEBIT));
        Assertions.assertEquals(ErrorCode.ERR_05,customException.getEnumCode());

        saveUpdateRequest.setAmount(new BigDecimal("3000.01"));
        saveUpdateRequest.setPlayerName("denis");
        Assertions.assertThrows(CustomException.class, () -> this.transactionLogService.saveTransactionLog(saveUpdateRequest,
                TransactionOperation.DEBIT));
        customException = Assertions.assertThrows(CustomException.class, () -> this.transactionLogService.saveTransactionLog(saveUpdateRequest,
                TransactionOperation.DEBIT));
        Assertions.assertEquals(ErrorCode.ERR_08,customException.getEnumCode());

        saveUpdateRequest.setPlayerName("denis");
        saveUpdateRequest.setAmount(new BigDecimal("3000"));
        Assertions.assertDoesNotThrow(() -> this.transactionLogService.saveTransactionLog(saveUpdateRequest,
                TransactionOperation.DEBIT));
        TransactionLogDTO transactionLogDTO = this.transactionLogService.findTransactionLogById("3");
        TransactionDTO transactionDTO = this.transactionService.findTransactionByPlayerName("denis");
        Assertions.assertNotNull(transactionLogDTO);
        Assertions.assertNotNull(transactionDTO);
        Assertions.assertEquals(TransactionOperation.DEBIT,transactionLogDTO.getOperation());
        Assertions.assertEquals(3000,transactionLogDTO.getAmount().intValue());
        Assertions.assertEquals(0,transactionDTO.getAmount().intValue());
        Assertions.assertNotNull(transactionDTO.getBaseModel().getUpdateDate());

    }

}

