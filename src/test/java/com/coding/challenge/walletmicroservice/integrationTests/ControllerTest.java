package com.coding.challenge.walletmicroservice.integrationTests;

import com.coding.challenge.walletmicroservice.configuration.ConstantsMessage;
import com.coding.challenge.walletmicroservice.controller.HandlerExceptionController;
import com.coding.challenge.walletmicroservice.controller.TransactionController;
import com.coding.challenge.walletmicroservice.controller.request.SaveUpdateRequest;
import com.coding.challenge.walletmicroservice.controller.response.GenericResponse;
import com.coding.challenge.walletmicroservice.model.mapper.TransactionLogMapper;
import com.coding.challenge.walletmicroservice.model.mapper.TransactionMapper;
import com.coding.challenge.walletmicroservice.service.TransactionLogService;
import com.coding.challenge.walletmicroservice.service.TransactionService;
import com.coding.challenge.walletmicroservice.service.implementation.TransactionServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.method.annotation.ExceptionHandlerMethodResolver;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@WebMvcTest
@ExtendWith(SpringExtension.class)
@Import({TransactionServiceImpl.class, TransactionMapper.class, TransactionLogMapper.class, ConstantsMessage.class, HandlerExceptionController.class})
public class ControllerTest {

    private MockMvc mockMvc;

    @MockBean
    private TransactionLogService transactionLogService;

    @MockBean
    private TransactionService transactionService;

    @InjectMocks
    private TransactionController transactionController;

    private JacksonTester<GenericResponse> jsonSuperHero;


    private static Map<String,String> errorMessages;

    @BeforeEach
    public void init(){
        errorMessages = new HashMap<>();
        errorMessages.put("ERR_01","Transaction id field is missing or is not valid (Max length = 10)!");
        errorMessages.put("ERR_02","The player name is missing or is not valid (Max length = 10)!");
        errorMessages.put("ERR_03","Amount field is missing or is not valid(<9 digits>.<2digits> expected)!");

        JacksonTester.initFields(this, new ObjectMapper());

        mockMvc = MockMvcBuilders.standaloneSetup(transactionController)
                .setHandlerExceptionResolvers(withExceptionControllerAdvice())
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();

    }

    @Test
    public void testHandleMethodArgumentNotValidMethod() throws Exception {

        SaveUpdateRequest saveUpdateRequest = new SaveUpdateRequest();
        MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.post("/api/transactions/credit",saveUpdateRequest)).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();
                //.andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(errorMessages)));


    }

    private ExceptionHandlerExceptionResolver withExceptionControllerAdvice() {
        final ExceptionHandlerExceptionResolver exceptionResolver = new ExceptionHandlerExceptionResolver() {
            @Override
            protected ServletInvocableHandlerMethod getExceptionHandlerMethod(final HandlerMethod handlerMethod,
                                                                              final Exception exception) {
                Method method = new ExceptionHandlerMethodResolver(HandlerExceptionController.class).resolveMethod(exception);
                if (method != null) {
                    return new ServletInvocableHandlerMethod(new HandlerExceptionController(), method);
                }
                return super.getExceptionHandlerMethod(handlerMethod, exception);
            }
        };
        exceptionResolver.afterPropertiesSet();
        return exceptionResolver;
    }
}
