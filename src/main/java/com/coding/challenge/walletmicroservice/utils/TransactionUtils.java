package com.coding.challenge.walletmicroservice.utils;

import com.coding.challenge.walletmicroservice.controller.request.SaveUpdateRequest;
import com.coding.challenge.walletmicroservice.controller.response.ErrorCode;
import com.coding.challenge.walletmicroservice.exception.ValidationException;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Deffo Denis
 * @version 1.0
 * Class responsible for validation
 */

public class TransactionUtils {

    public static boolean checkEmptyList(List objectList) {
        return CollectionUtils.isEmpty(objectList);
    }

    public static boolean checkEmptyString(String object) {
        return object == null || object.trim().isEmpty();
    }

    public static void validateSaveUpdateRequest(SaveUpdateRequest saveUpdateRequest) throws ValidationException,Exception {
        List<Enum> enumList = new ArrayList<>();
        boolean errorFound = false;
        if(TransactionUtils.checkEmptyString(saveUpdateRequest.getTransactionLogId())){
            enumList.add(ErrorCode.ERR_01);
            errorFound = true;
        }
        if(TransactionUtils.checkEmptyString(saveUpdateRequest.getPlayerName())){
            enumList.add(ErrorCode.ERR_02);
            errorFound = true;
        }
        if(saveUpdateRequest.getAmount() == null){
            enumList.add(ErrorCode.ERR_03);
            errorFound = true;
        }
        if(errorFound)
            throw new ValidationException(enumList);
    }

    public static Map<String,String> constructError(String errorCodeString , List<String> errorList){
        Map<String,String> errorMap = new HashMap<>();
        errorList.forEach(message -> {
            String[] messageSplit = message.split(",");
            if(errorCodeString.equals(messageSplit[0])){
                errorMap.put(errorCodeString,messageSplit[1]);
            }
        });
        return errorMap;
    }

    public static void trimRequest(SaveUpdateRequest saveUpdateRequest){
        saveUpdateRequest.setPlayerName(trimLowerCaseString(saveUpdateRequest.getPlayerName()));
        saveUpdateRequest.setTransactionLogId(trimLowerCaseString(saveUpdateRequest.getTransactionLogId()));
    }

    public static String trimLowerCaseString(String object){
        return object.trim().toLowerCase();
    }

    public static void validateHistoryRequest(String playerName){
        if(TransactionUtils.checkEmptyString(playerName)){
            List<Enum> enumList = new ArrayList<>();
            enumList.add(ErrorCode.ERR_02);
            throw new ValidationException(enumList);
        }

    }

}
