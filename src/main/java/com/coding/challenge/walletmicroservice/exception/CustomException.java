package com.coding.challenge.walletmicroservice.exception;

import lombok.Data;

/**
 * @author Deffo Denis
 * @version 1.0
 * Any Exception related to database operations
 */

@Data
public class CustomException extends RuntimeException {

    private Enum enumCode;

    private String message;

    public CustomException(){}

    public CustomException(String message) {
        super(message);
    }

    public CustomException(Enum enumCode) {
        this.enumCode = enumCode;
    }

}

