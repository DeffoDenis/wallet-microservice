package com.coding.challenge.walletmicroservice.exception;

import lombok.Data;

import java.util.List;

/**
 * @author Deffo Denis
 * @version 1.0
 * Any Exception related to objects request Validation
 */

@Data
public class ValidationException extends RuntimeException {

    private Enum enumCode;

    private List<Enum> enumList;

    public ValidationException(){}

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(Enum enumCode) {
        this.enumCode = enumCode;
    }

    public ValidationException(List<Enum> enumList) {
        this.enumList = enumList;
    }
}
