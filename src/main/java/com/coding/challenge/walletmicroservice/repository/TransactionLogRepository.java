package com.coding.challenge.walletmicroservice.repository;

import com.coding.challenge.walletmicroservice.model.TransactionLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Deffo Denis
 * @version 1.0
 * This interface extends the JPA repository and
 * leverages queries operations for wallet_transaction_log table .
 */
@Repository
public interface TransactionLogRepository extends JpaRepository<TransactionLog,String> {

    TransactionLog findByTransactionLogId(String transactionLogId) throws Exception;
    List<TransactionLog> findByPlayerName(String playerName);
}
