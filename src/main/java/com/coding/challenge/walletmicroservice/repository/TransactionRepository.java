package com.coding.challenge.walletmicroservice.repository;

import com.coding.challenge.walletmicroservice.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Deffo Denis
 * @version 1.0
 * This interface extends the JPA repository and
 * leverages queries operations for wallet_transaction table .
 */
@Repository
public interface TransactionRepository extends JpaRepository<Transaction,Long> {

    List<Transaction> findByPlayerName(String playerName);
}
