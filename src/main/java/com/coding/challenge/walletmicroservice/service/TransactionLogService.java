package com.coding.challenge.walletmicroservice.service;

import com.coding.challenge.walletmicroservice.controller.request.SaveUpdateRequest;
import com.coding.challenge.walletmicroservice.model.TransactionOperation;
import com.coding.challenge.walletmicroservice.model.dto.TransactionLogDTO;

import java.util.List;

/**
 * @author Deffo Denis
 * @version 1.0
 */
public interface TransactionLogService {

    /**
     *
     * @param id
     * @return
     * @throws Exception
     */
    TransactionLogDTO findTransactionLogById(String id) throws Exception;

    /**
     *
     * @param playerName
     * @return
     * @throws Exception
     */
    List<TransactionLogDTO> findTransactionsLogByPlayerName(String playerName) throws  Exception;

    /**
     *
     * @param saveUpdateRequest
     * @param transactionOperation
     * @throws Exception
     */
    void saveTransactionLog(SaveUpdateRequest saveUpdateRequest , TransactionOperation transactionOperation) throws Exception;
}
