package com.coding.challenge.walletmicroservice.service.implementation;

import com.coding.challenge.walletmicroservice.controller.request.SaveUpdateRequest;
import com.coding.challenge.walletmicroservice.controller.response.ErrorCode;
import com.coding.challenge.walletmicroservice.exception.CustomException;
import com.coding.challenge.walletmicroservice.model.Transaction;
import com.coding.challenge.walletmicroservice.model.TransactionLog;
import com.coding.challenge.walletmicroservice.model.TransactionOperation;
import com.coding.challenge.walletmicroservice.model.dto.TransactionDTO;
import com.coding.challenge.walletmicroservice.model.dto.TransactionLogDTO;
import com.coding.challenge.walletmicroservice.model.mapper.TransactionLogMapper;
import com.coding.challenge.walletmicroservice.model.mapper.TransactionMapper;
import com.coding.challenge.walletmicroservice.repository.TransactionLogRepository;
import com.coding.challenge.walletmicroservice.repository.TransactionRepository;
import com.coding.challenge.walletmicroservice.service.TransactionLogService;
import com.coding.challenge.walletmicroservice.service.TransactionService;
import com.coding.challenge.walletmicroservice.utils.TransactionUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Deffo Denis
 * @version 1.0
 * Service core, layer between controller and Repositories
 */
@Service
@Slf4j
public class TransactionServiceImpl implements TransactionService , TransactionLogService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionLogRepository transactionLogRepository;

    @Autowired
    private TransactionMapper transactionMapper;

    @Autowired
    private TransactionLogMapper transactionLogMapper;

    @Override
    public TransactionDTO findTransactionById(Long id) throws Exception {
        return transactionRepository.findById(id)
                .map(transaction -> this.transactionMapper.toDTO(transaction))
                .orElse(null);
    }

    @Override
    public TransactionDTO findTransactionByPlayerName(String playerName)  throws Exception {
        List<Transaction> transactionList = transactionRepository.findByPlayerName(playerName);
        if(!TransactionUtils.checkEmptyList(transactionList))
            return this.transactionMapper.toDTO(transactionList.get(0));
        else
            return null;
    }

    @Override
    public TransactionLogDTO findTransactionLogById(String transactionLogId) throws Exception {
        try{
            return Optional.ofNullable(this.transactionLogRepository.findByTransactionLogId(transactionLogId))
                    .map(transactionLog -> this.transactionLogMapper.toDTO(transactionLog))
                    .orElseGet(null);
        } catch (NullPointerException nullPointerException){
            return null;
        }

    }

    @Override
    public List<TransactionLogDTO> findTransactionsLogByPlayerName(String playerName) throws Exception {
        return Optional.ofNullable(this.transactionLogRepository.findByPlayerName(playerName))
                .map(transactionLogs -> transactionLogs.stream().map(
                        transactionLog -> this.transactionLogMapper.toDTO(transactionLog)
                ).collect(Collectors.toList()))
                .orElseGet(null);
    }

    @Transactional
    @Override
    public void saveTransactionLog(SaveUpdateRequest saveUpdateRequest, TransactionOperation transactionOperation) throws Exception {

        if(!Optional.ofNullable(this.findTransactionLogById(saveUpdateRequest.getTransactionLogId())).isPresent()){

            TransactionDTO transactionDTO = this.findTransactionByPlayerName(saveUpdateRequest.getPlayerName());

            TransactionLogDTO transactionLogDTO = this.transactionLogMapper.toSaveUpdateRequest(saveUpdateRequest);
            transactionLogDTO.setAmount(saveUpdateRequest.getAmount());
            transactionLogDTO.setOperation(transactionOperation);

            switch (transactionOperation) {
                case DEBIT:
                    if(transactionDTO == null) {
                        throw new CustomException(ErrorCode.ERR_05);
                    }if(saveUpdateRequest.getAmount().compareTo(transactionDTO.getAmount()) > 0) {
                        throw new CustomException(ErrorCode.ERR_08);
                    } else {
                        transactionDTO.setAmount(transactionDTO.getAmount().subtract(saveUpdateRequest.getAmount()));
                        break;
                    }
                default:
                    if(transactionDTO == null) {
                        transactionDTO = this.transactionMapper.fromLog(transactionLogDTO);
                    } else {
                        transactionDTO.setAmount(transactionDTO.getAmount().add(saveUpdateRequest.getAmount()));
                    }
                    break;
            }
            this.saveTransactionLog(transactionLogDTO);
            this.saveTransaction(transactionDTO);

        } else {
            throw new CustomException(ErrorCode.ERR_04);
        }
    }

    @Override
    public BigDecimal findCurrentBalanceByPlayer(String playerName) throws Exception {
        try {
            return Optional.ofNullable(this.transactionRepository.findByPlayerName(playerName))
                    .map(transactions -> transactions.get(0).getAmount())
                    .orElseThrow(() -> new CustomException(ErrorCode.ERR_05));
        } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
            throw new CustomException(ErrorCode.ERR_05);
        }
    }

    private void saveTransaction(TransactionDTO transactionDTO) throws Exception {
        Transaction transaction = this.transactionMapper.toModel(transactionDTO);
        this.transactionRepository.save(transaction);
    }

    private void saveTransactionLog(TransactionLogDTO transactionLogDTO) throws Exception {
        TransactionLog transactionLog = this.transactionLogMapper.toModel(transactionLogDTO);
        this.transactionLogRepository.save(transactionLog);
    }

}
