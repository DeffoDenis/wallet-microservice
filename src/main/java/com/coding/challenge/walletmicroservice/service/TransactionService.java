package com.coding.challenge.walletmicroservice.service;

import com.coding.challenge.walletmicroservice.model.dto.TransactionDTO;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Deffo Denis
 * @version 1.0
 */
public interface TransactionService {

    /**
     *
     * @param id
     * @return
     * @throws Exception
     */
    TransactionDTO findTransactionById(Long id) throws Exception;

    /**
     *
     * @param playerName
     * @return
     * @throws Exception
     */
    TransactionDTO findTransactionByPlayerName(String playerName) throws  Exception;

    /**
     *
     * @param player
     * @return
     * @throws Exception
     */
    BigDecimal findCurrentBalanceByPlayer(String player) throws Exception;
}
