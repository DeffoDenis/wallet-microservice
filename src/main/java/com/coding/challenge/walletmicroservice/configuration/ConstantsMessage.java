package com.coding.challenge.walletmicroservice.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Deffo denis
 * @version 1.0
 */

@Data
@Component
@Configuration
@ConfigurationProperties
@PropertySource(value = "classpath:messages.yaml" ,factory = ConstantsFactory.class)
public class ConstantsMessage {

    private List<String> errorMessages;
    private String creditTransactionSuccess;
    private String debitTransactionSuccess;

}
