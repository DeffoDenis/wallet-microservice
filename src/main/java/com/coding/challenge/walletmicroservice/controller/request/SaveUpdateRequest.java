package com.coding.challenge.walletmicroservice.controller.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.math.BigDecimal;

/**
 * @author Deffo Denis
 * @version 1.0
 * Object body for credit/debit request , it is mandatory
 * and its fields are validated
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaveUpdateRequest {

    @Schema(description = "Player name",
            example = "denis", required = true)
    @NotBlank(message = "ERR_02")
    @Size(max=10,message ="ERR_02")
    @Pattern(regexp ="^[a-zA-Z0-9]+$" ,message ="ERR_02")
    private String playerName;

    @Schema(description = "amount account",
            example = "5000", required = true)
    @Digits(integer = 9, fraction = 2,message = "ERR_03")
    private BigDecimal amount;


    @Schema(description = "id transaction, must be unique",
            example = "4", required = true)
    @NotBlank(message = "ERR_01")
    @Size(max=10,message ="ERR_01")
    private String transactionLogId;
}
