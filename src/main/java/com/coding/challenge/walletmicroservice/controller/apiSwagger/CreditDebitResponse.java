package com.coding.challenge.walletmicroservice.controller.apiSwagger;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author Deffo Denis
 * @version 1.0
 * FOr Swagger purpose, object returned after successfull credit/debit requests
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreditDebitResponse {

    @Schema(description = "Success message ",
            example = "Credit operation successfull!")
    private String successMessage;
}
