package com.coding.challenge.walletmicroservice.controller.response;

import com.coding.challenge.walletmicroservice.model.TransactionLog;
import com.coding.challenge.walletmicroservice.model.dto.TransactionLogDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author Deffo Denis
 * @version 1.0
 * Object response for all requests
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GenericResponse implements Serializable {

    @Schema
    private List<TransactionLogDTO> transactions;
    @Schema
    private BigDecimal currentBalance;
    @Schema
    private String successMessage;
    @Schema
    private Map<String,String> errorMessages;

}
