package com.coding.challenge.walletmicroservice.controller.apiSwagger;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Deffo Denis
 * @version 1.0
 * For Swagger purpose, object returned after successfull currentBalance requests
 */

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CurrentBalanceResponse {

    @Schema(description = "Success message ",
            example = "3000")
    private BigDecimal currentBalance;
}
