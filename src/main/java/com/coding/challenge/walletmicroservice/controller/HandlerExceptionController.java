package com.coding.challenge.walletmicroservice.controller;

import com.coding.challenge.walletmicroservice.configuration.ConstantsMessage;
import com.coding.challenge.walletmicroservice.controller.apiSwagger.ErrorMessagesResponse;
import com.coding.challenge.walletmicroservice.controller.response.ErrorCode;
import com.coding.challenge.walletmicroservice.controller.response.GenericResponse;
import com.coding.challenge.walletmicroservice.exception.CustomException;
import com.coding.challenge.walletmicroservice.exception.ValidationException;
import com.coding.challenge.walletmicroservice.utils.TransactionUtils;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Deffo Denis
 * @version 1.0
 * This class handles any exception occured and returns a response
 * with error codes and messages.
 */
@ControllerAdvice
public class HandlerExceptionController extends ResponseEntityExceptionHandler {

    @Autowired
    public ConstantsMessage constantsMessage;

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        GenericResponse generateResponse = this.initializeGenericResponse();
        Map<String ,String> errorMap = this.messageValidationGetMethod(ex.getLocalizedMessage());
        if(errorMap != null){
            generateResponse.getErrorMessages().putAll(errorMap);
            return new ResponseEntity(generateResponse, HttpStatus.BAD_REQUEST);
        } else {
            generateResponse.getErrorMessages().put(ErrorCode.ERR_06.name() ,ex.getLocalizedMessage());
            return new ResponseEntity(generateResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ExceptionHandler(CustomException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<Object> handleCustomException(CustomException customException, WebRequest request) {
        GenericResponse generateResponse = this.initializeGenericResponse();
        generateResponse.getErrorMessages().putAll(TransactionUtils.constructError(customException.getEnumCode().name(), constantsMessage.getErrorMessages()));
        return new ResponseEntity(generateResponse, HttpStatus.NOT_FOUND);
    }

    @Override
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException methodArgumentNotValidException, HttpHeaders headers, HttpStatus status, WebRequest request) {
        GenericResponse generateResponse = this.initializeGenericResponse();
        for(ObjectError error : methodArgumentNotValidException.getBindingResult().getAllErrors()) {
            generateResponse.getErrorMessages().putAll(TransactionUtils.constructError(error.getDefaultMessage(), constantsMessage.getErrorMessages()));
        }
        return new ResponseEntity(generateResponse, HttpStatus.BAD_REQUEST);
    }

    @Override
    @ApiResponse(responseCode = "405", description = "Method Not allowed",
            content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorMessagesResponse.class))})
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        GenericResponse generateResponse = this.initializeGenericResponse();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(
                " The method you used is not supported for this request.\n Here is the supported method:");
        ex.getSupportedHttpMethods().forEach(str -> stringBuilder.append(str+" "));
        generateResponse.getErrorMessages().put(ErrorCode.ERR_06.name(),stringBuilder.toString());
        return new ResponseEntity(generateResponse, HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler(ValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final ResponseEntity<Object> handleValidationException(ValidationException validationException, WebRequest request) {
        GenericResponse generateResponse = this.initializeGenericResponse();
        validationException.getEnumList().forEach(anEnum -> {
            generateResponse.getErrorMessages().putAll(TransactionUtils.constructError(anEnum.name(), constantsMessage.getErrorMessages()));
        });
        return new ResponseEntity(generateResponse, HttpStatus.BAD_REQUEST);
    }

    private GenericResponse initializeGenericResponse(){
        GenericResponse generateResponse = new GenericResponse();
        generateResponse.setErrorMessages(new HashMap<>());
        return generateResponse;
    }

    private Map<String,String> messageValidationGetMethod(String exceptionMessage){
        if(exceptionMessage != null){
            String[] splitMessage = exceptionMessage.split(":");
            if(splitMessage != null && splitMessage.length > 1
                    && splitMessage[1].trim().equals(ErrorCode.ERR_02.name())){
                 return TransactionUtils.constructError(ErrorCode.ERR_02.name(), constantsMessage.getErrorMessages());
            }
        }
        return null;
    }

}
