package com.coding.challenge.walletmicroservice.controller;

import com.coding.challenge.walletmicroservice.configuration.ConstantsMessage;
import com.coding.challenge.walletmicroservice.controller.apiSwagger.CreditDebitResponse;
import com.coding.challenge.walletmicroservice.controller.apiSwagger.CurrentBalanceResponse;
import com.coding.challenge.walletmicroservice.controller.apiSwagger.ErrorMessagesResponse;
import com.coding.challenge.walletmicroservice.controller.request.SaveUpdateRequest;
import com.coding.challenge.walletmicroservice.controller.response.GenericResponse;
import com.coding.challenge.walletmicroservice.exception.CustomException;
import com.coding.challenge.walletmicroservice.exception.ValidationException;
import com.coding.challenge.walletmicroservice.model.TransactionOperation;
import com.coding.challenge.walletmicroservice.model.dto.TransactionLogDTO;
import com.coding.challenge.walletmicroservice.service.TransactionLogService;
import com.coding.challenge.walletmicroservice.service.TransactionService;
import com.coding.challenge.walletmicroservice.utils.TransactionUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * @author Deffo Denis
 * @version 1.0
 * Application entryPoint for Requests
 */
@RestController
@RequestMapping("api/transactions")
@Slf4j
@Validated
@Tag(name = "Transactions", description = "WALLET MICROSERVICE API")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private TransactionLogService transactionLogService;

    @Autowired
    private ConstantsMessage constantsMessage;

    /**
     * H
     * @param plaYerName
     * @return ResponseEntity
     * @throws Exception
     */
    @Operation(summary = "Current balance per player")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found current balance",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CurrentBalanceResponse.class))}),
            @ApiResponse(responseCode = "400", description = " format Player name not valid",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessagesResponse.class))}),
            @ApiResponse(responseCode = "404", description = "Player name not exists",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessagesResponse.class))}),
            @ApiResponse(responseCode = "500", description = "Internal Server Error",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessagesResponse.class))})
    })
    @GetMapping("/balance/{playerName}")
    public ResponseEntity currentBalancePerPlayer(@Parameter(description="Player name Cannot null or empty.",
            required=true, schema=@Schema(implementation = String.class))
            @PathVariable("playerName") @NotBlank(message = "ERR_02") @Size(max=10,message ="ERR_02") @Pattern(regexp ="^[a-zA-Z0-9]+$") String plaYerName) throws Exception {
        TransactionUtils.validateHistoryRequest(plaYerName);
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setCurrentBalance(this.transactionService.findCurrentBalanceByPlayer(TransactionUtils.trimLowerCaseString(plaYerName)));
        return new ResponseEntity(genericResponse,HttpStatus.OK);
    }


    /**
     *
     * @param plaYerName
     * @return ResponseEntity
     * @throws Exception
     */
    @Operation(summary = "Transaction history per player")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found transactions history or Empty list",
                    content = { @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = TransactionLogDTO.class)))}),
            @ApiResponse(responseCode = "400", description = " format Player name not valid",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessagesResponse.class))}),
            @ApiResponse(responseCode = "404", description = "Player name not exists",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessagesResponse.class))}),
            @ApiResponse(responseCode = "500", description = "Internal Server Error",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessagesResponse.class))})
    })
    @GetMapping("/history/{playerName}")
    @Validated
    public ResponseEntity transactionHistoryPerPlayer(@Parameter(description="Player name Cannot null or empty.",
            required=true, schema=@Schema(implementation = String.class))
            @PathVariable("playerName") @NotBlank(message = "ERR_02") @Size(max=10,message ="ERR_02") @Pattern(regexp ="^[a-zA-Z0-9]+$") String plaYerName) throws Exception {
        TransactionUtils.validateHistoryRequest(plaYerName);
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setTransactions(this.transactionLogService.findTransactionsLogByPlayerName(TransactionUtils.trimLowerCaseString(plaYerName)));
        return new ResponseEntity(genericResponse,HttpStatus.OK);
    }

    /**
     *
     * @param saveUpdateRequest
     * @return ResponseEntity
     */
    @Operation(summary = "Credit per player. The caller will supply a transaction id that must be unique for all transactions")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "A new transaction record and log have been created",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreditDebitResponse.class))}),
            @ApiResponse(responseCode = "400", description = " format Player name not valid, " +
                    "transaction id is null or format transaction not valid, amount is null or format amount is not valid",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessagesResponse.class))}),
            @ApiResponse(responseCode = "404", description = "Transaction id is not unique",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessagesResponse.class))}),
            @ApiResponse(responseCode = "500", description = "Internal Server Error",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessagesResponse.class))})
    })
    @PostMapping("/credit")
    public ResponseEntity creditTransaction(@Parameter(description="Player name, amount , transaction log id(not duplicate) Cannot null or empty.",
            required=true, schema=@Schema(implementation = SaveUpdateRequest.class))
            @RequestBody @Valid SaveUpdateRequest saveUpdateRequest) throws Exception, CustomException, ValidationException {
        return this.saveDebitOperation(saveUpdateRequest,false);
    }

    /**
     *
     * @param saveUpdateRequest
     * @return ResponseEntity
     * @throws Exception
     */
    @Operation(summary = "Debit per player. The caller will supply a transaction id that must be unique for all transactions")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "A transaction record has been updated and log has been created",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreditDebitResponse.class))}),
            @ApiResponse(responseCode = "400", description = " format Player name not valid, " +
                    "transaction id is null or format transaction not valid, amount is null or format amount is not valid",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessagesResponse.class))}),
            @ApiResponse(responseCode = "404", description = "Transaction id is not unique,amount is greater than current balance," +
                    "player name not exists",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessagesResponse.class))}),
            @ApiResponse(responseCode = "500", description = "Internal Server Error",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorMessagesResponse.class))})
    })
    @PostMapping("/debit")
    public ResponseEntity debitTransaction(@Parameter(description="Player name, amount , transaction log id(not duplicate) Cannot null or empty.",
            required=true, schema=@Schema(implementation = SaveUpdateRequest.class))
            @RequestBody @Valid SaveUpdateRequest saveUpdateRequest) throws Exception, CustomException, ValidationException {
        return this.saveDebitOperation(saveUpdateRequest,true);
    }

    private ResponseEntity saveDebitOperation(SaveUpdateRequest saveUpdateRequest,boolean debit) throws Exception {
        TransactionUtils.validateSaveUpdateRequest(saveUpdateRequest);
        TransactionUtils.trimRequest(saveUpdateRequest);
        GenericResponse genericResponse = new GenericResponse();
        if(debit) {
            this.transactionLogService.saveTransactionLog(saveUpdateRequest, TransactionOperation.DEBIT);
            genericResponse.setSuccessMessage(constantsMessage.getDebitTransactionSuccess());
            return new ResponseEntity(genericResponse, HttpStatus.OK);
        }
        this.transactionLogService.saveTransactionLog(saveUpdateRequest, TransactionOperation.CREDIT);
        genericResponse.setSuccessMessage(constantsMessage.getCreditTransactionSuccess());
        return new ResponseEntity(genericResponse, HttpStatus.CREATED);

    }

}
