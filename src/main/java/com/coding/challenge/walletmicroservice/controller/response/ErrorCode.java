package com.coding.challenge.walletmicroservice.controller.response;


/**
 * @author Deffo Denis
 * @version 1.0
 * Error code sent after validation errors.
 */
public enum ErrorCode {

    ERR_01("ERR_01"),
    ERR_02("ERR_02"),
    ERR_03("ERR_03"),
    ERR_04("ERR_04"),
    ERR_05("ERR_05"),
    ERR_06("ERR_06"),
    ERR_07("ERR_07"),
    ERR_08("ERR_08");

    private String code;

    ErrorCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
