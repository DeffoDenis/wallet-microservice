package com.coding.challenge.walletmicroservice.controller.apiSwagger;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Map;

/**
 * @author Deffo Denis
 * @version 1.0
 * For Swagger purpose, object returned after application errors
 */

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorMessagesResponse {

    @Schema(description = "Message errors List")
    private Map<String,String> errorMessages;
}
