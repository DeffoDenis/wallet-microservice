package com.coding.challenge.walletmicroservice.model;

import com.coding.challenge.walletmicroservice.model.listener.DateListener;
import com.coding.challenge.walletmicroservice.model.listener.DateService;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Deffo Denis
 * @version 1.0
 * Transaction Entity class(wallet_transaction table).
 * This table is a snapshot of the most recent version of the player account
 */

@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@EntityListeners(DateListener.class)
@Table(name = "wallet_transaction")
@Entity
public class Transaction implements DateService,Serializable {

    @Id
    @SequenceGenerator(name= "WALLET_TRANSACTION_SEQUENCE", sequenceName = "WALLET_TRANSACTION_SEQUENCE_ID", initialValue=1, allocationSize = 1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="WALLET_TRANSACTION_SEQUENCE")
    private Long id;
    @Column(name = "amount")
    private BigDecimal amount;
    @Size(max=10)
    @Pattern(regexp ="^[a-zA-Z0-9]+$")
    @Column(name = "player_name")
    private String playerName;
    @Embedded
    private BaseModel baseModel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    @Override
    public BaseModel getBaseModel() {
        return baseModel;
    }

    @Override
    public void setBaseModel(BaseModel baseModel) {
        this.baseModel = baseModel;
    }
}
