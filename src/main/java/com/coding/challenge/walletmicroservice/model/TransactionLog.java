package com.coding.challenge.walletmicroservice.model;

import com.coding.challenge.walletmicroservice.model.listener.DateListener;
import com.coding.challenge.walletmicroservice.model.listener.DateService;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * @author Deffo Denis
 * @version 1.0
 * Transaction log Entity class(wallet_transaction_log table).
 * This table is kind of storage of all operations made by users.
 */

@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@EntityListeners(DateListener.class)
@Table(name = "wallet_transaction_log")
@Entity
public class TransactionLog implements DateService{

    @Id
    @Size(max=10)
    @Pattern(regexp ="^[a-zA-Z0-9]+$")
    @Column(name = "Transaction_log_id")
    private String transactionLogId;
    @Digits(integer = 9, fraction = 2)
    @Column(name = "amount")
    private BigDecimal amount;
    @NotBlank
    @Size(max=10)
    @Pattern(regexp ="^[a-zA-Z0-9]+$")
    @Column(name = "player_name")
    private String playerName;
    @Enumerated(EnumType.STRING)
    @Column(name = "operation")
    private TransactionOperation operation;
    @Embedded
    private BaseModel baseModel;

    public String getTransactionLogId() {
        return transactionLogId;
    }

    public void setTransactionLogId(String transactionLogId) {
        this.transactionLogId = transactionLogId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public TransactionOperation getOperation() {
        return operation;
    }

    public void setOperation(TransactionOperation operation) {
        this.operation = operation;
    }

    @Override
    public BaseModel getBaseModel() {
        return baseModel;
    }

    @Override
    public void setBaseModel(BaseModel baseModel) {
        this.baseModel = baseModel;
    }
}
