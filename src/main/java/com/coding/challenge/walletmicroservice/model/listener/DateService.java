package com.coding.challenge.walletmicroservice.model.listener;

import com.coding.challenge.walletmicroservice.model.BaseModel;

/**
 * @author Deffo Denis
 * @version 1.0
 */
public interface DateService {

    BaseModel getBaseModel();
    void setBaseModel(BaseModel baseModel);
}
