package com.coding.challenge.walletmicroservice.model.mapper;

/**
 *
 * @param <T>
 * @param <K>
 */
public interface Mapper<T,K> {

    /**
     *
     * @param model
     * @return
     * @throws Exception
     */
    T toDTO(K model);

    /**
     *
     * @param dto
     * @return
     * @throws Exception
     */
    K toModel(T dto);
}
