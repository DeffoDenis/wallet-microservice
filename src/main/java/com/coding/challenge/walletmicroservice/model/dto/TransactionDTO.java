package com.coding.challenge.walletmicroservice.model.dto;


import com.coding.challenge.walletmicroservice.model.BaseModel;
import lombok.*;

import java.math.BigDecimal;


/**
 * @author Deffo Denis
 * @version 1.0
 * DTO class for transactions entity
 */

@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class TransactionDTO implements BaseDTO {

    private Long id;
    private BigDecimal amount;
    private String playerName;
    private BaseModel baseModel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public BaseModel getBaseModel() {
        return baseModel;
    }

    public void setBaseModel(BaseModel baseModel) {
        this.baseModel = baseModel;
    }
}
