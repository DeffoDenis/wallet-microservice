package com.coding.challenge.walletmicroservice.model;

/**
 * @author Deffo Denis
 * @version 1.0
 */
public enum TransactionOperation {
    DEBIT,
    CREDIT
}
