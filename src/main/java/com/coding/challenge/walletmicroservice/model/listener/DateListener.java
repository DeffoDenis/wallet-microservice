package com.coding.challenge.walletmicroservice.model.listener;

import com.coding.challenge.walletmicroservice.model.BaseModel;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author Deffo Denis
 * @version 1.0
 * This class handles JPA entity auditing, modifies updateDate and InsertDate
 * before any new insertion or update
 */
public class DateListener {

    // Before updating existing transaction records in database , we need to update the updateDate field
    @PreUpdate
    public void setUpdateDate(DateService dateService){
        BaseModel baseModel = dateService.getBaseModel() != null ? dateService.getBaseModel() : new BaseModel();
        baseModel.setUpdateDate(new Timestamp(new Date().getTime()));
        dateService.setBaseModel(baseModel);
    }

    // Before saving new transaction/transaction logs records in database , we need to update the insertDate field
    @PrePersist
    public void setInsertDate(DateService dateService){
        BaseModel baseModel = dateService.getBaseModel() != null ? dateService.getBaseModel() : new BaseModel();
        baseModel.setInsertDate(new Timestamp(new Date().getTime()));
        dateService.setBaseModel(baseModel);
    }
}
