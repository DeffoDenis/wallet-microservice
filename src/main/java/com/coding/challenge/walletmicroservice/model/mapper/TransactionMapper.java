package com.coding.challenge.walletmicroservice.model.mapper;

import com.coding.challenge.walletmicroservice.model.Transaction;
import com.coding.challenge.walletmicroservice.model.TransactionLog;
import com.coding.challenge.walletmicroservice.model.dto.TransactionDTO;
import com.coding.challenge.walletmicroservice.model.dto.TransactionLogDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

/**
 * @author Deffo Denis
 * @version 1.0
 * Mapper for Transacton logs
 */
@Component
public class TransactionMapper implements Mapper<TransactionDTO , Transaction>{

    /**
     * Transforms a Transaction model in a DTO
     * @param model
     * @return TransactionDTO
     */
    @Override
    public TransactionDTO toDTO(Transaction model){
        TransactionDTO target = new TransactionDTO();
        BeanUtils.copyProperties(model,target);
        return target;
    }

    /**
     * Transforms a Transaction DTO in a model
     * @param dto
     * @return Transaction
     */
    @Override
    public Transaction toModel(TransactionDTO dto){
        Transaction target = new Transaction();
        BeanUtils.copyProperties(dto,target);
        return target;
    }

    public TransactionDTO fromLog(TransactionLogDTO transactionLogDTO){
        TransactionDTO target = new TransactionDTO();
        BeanUtils.copyProperties(transactionLogDTO ,target,"updateDate", "id", "operation");
        return target;
    }
}
