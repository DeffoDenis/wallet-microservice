package com.coding.challenge.walletmicroservice.model.mapper;

import com.coding.challenge.walletmicroservice.controller.request.SaveUpdateRequest;
import com.coding.challenge.walletmicroservice.model.Transaction;
import com.coding.challenge.walletmicroservice.model.TransactionLog;
import com.coding.challenge.walletmicroservice.model.dto.TransactionDTO;
import com.coding.challenge.walletmicroservice.model.dto.TransactionLogDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

/**
 * @author Deffo Denis
 * @version 1.0
 * Mapper for Transacton logs
 */
@Component
public class TransactionLogMapper implements Mapper<TransactionLogDTO, TransactionLog>{

    /**
     * Transforms a Transaction log model in a DTO
     * @param model
     * @return TransactionLogDTO
     */
    @Override
    public TransactionLogDTO toDTO(TransactionLog model){
        TransactionLogDTO target = new TransactionLogDTO();
        BeanUtils.copyProperties(model,target);
        return target;
    }

    /**
     * Transforms a Transaction log DTO in a model
     * @param dto
     * @return TransactionLog
     */
    @Override
    public TransactionLog toModel(TransactionLogDTO dto) {
        TransactionLog target = new TransactionLog();
        BeanUtils.copyProperties(dto,target);
        return target;
    }

    /**
     * Transforms a saveUpdateRequest object in DTO trasaction log
     * @param saveUpdateRequest
     * @return TransactionLogDTO
     */
    public TransactionLogDTO toSaveUpdateRequest(SaveUpdateRequest saveUpdateRequest) {
        TransactionLogDTO target = new TransactionLogDTO();
        BeanUtils.copyProperties(saveUpdateRequest,target);
        return target;
    }
}
