
CREATE TABLE IF NOT EXISTS wallet_transaction_log (
  transaction_log_id VARCHAR(250) PRIMARY KEY,
  player_name VARCHAR(250) NOT NULL,
  amount BIGINT NOT NULL,
  operation VARCHAR(250) NOT NULL,
  insert_date TIMESTAMP NOT NULL,
  update_date TIMESTAMP
);

CREATE TABLE IF NOT EXISTS wallet_transaction (
  id  INT  PRIMARY KEY AUTO_INCREMENT,
  player_name VARCHAR(250) NOT NULL,
  amount BIGINT NOT NULL,
  insert_date TIMESTAMP NOT NULL,
  update_date TIMESTAMP
);

CREATE SEQUENCE wallet_transaction_SEQUENCE_ID START WITH 1 INCREMENT BY 1;