# Wallet Microservice API

 A monetary account holds the current balance for a player. The balance can be modified by registering transactions on the account, either debit transactions (removing funds) or credit transactions (adding funds).
 Thi API has been developed with Spring-boot and its embedded H2 database in persisted mode. It was dockerized and pushed.
 Every app restart inside the container doesn't loose data.

## Installation

The only thing you need to install is Docker (in case you haven't)

```
[Install Docker](https://docs.docker.com/get-docker/)
```

The Application is started via Docker run:
```
docker run --name wallet -it -d -p 8080:8080 -t yvesdeffo/wallet-microservice:latest
```
The h2 database console can be accessed via :
```
http://localhost:8080/h2-console
The jdbc url is actually the location of the db files(wallet.mv.db) , just put this: jdbc:h2:./database-h2/wallet for the JDBC URL
User Name: user
Password : password

```

The API was documentated with OPEN-API and the url is:
```
http://localhost:8080/swagger-ui.html
```

## Usage

It has been used two tables for the microservice API:
- Wallet_transaction: This table contains records that are actually most recent account situations for every player. The id is the primary key but the payer name is unique.
- Wallet_transaction_id: This table keeps track of all credit/Debit transaction operations made by the application. Th primary key is the transaction id supplied by the request ,hence it is unique.

## REST-API
The REST API to the example app is described below
## Debit per player.
### Request
```
POST http://localhost:8080/api/transactions/debit

curl -X POST "http://localhost:8080/api/transactions/debit" -H  "accept: application/json" -H "Content-Type: application/json" -d "{\"playerName\":\"denis\",\"amount\":5000,\"transactionLogId\":\"1\"}"

```
 
### Response
```
connection: keep-alive 
 content-type: application/json 
 date: Mon,08 Mar 2021 18:17:04 GMT 
 keep-alive: timeout=60 
 transfer-encoding: chunked 
 vary: Origin,Access-Control-Request-Method,Access-Control-Request-Headers 

{
  "successMessage": "Debit operation successfull!"
}
```

## Credit per player.
### Request
```
POST http://localhost:8080/api/transactions/credit

curl -X POST "http://localhost:8080/api/transactions/credit" -H  "accept: application/json" -H "Content-Type: application/json" -d "{\"playerName\":\"denis\",\"amount\":5000,\"transactionLogId\":\"1\"}"

```
 
### Response
```
connection: keep-alive 
 content-type: application/json 
 date: Mon,08 Mar 2021 18:26:11 GMT 
 keep-alive: timeout=60 
 transfer-encoding: chunked 
 vary: Origin,Access-Control-Request-Method,Access-Control-Request-Headers 

{
  "successMessage": "Credit operation successfull!"
}
```

## transactions history per player.
### Request
```
GET http://localhost:8080/api/transactions/history/{playerName}

curl -X GET "http://localhost:8080/api/transactions/history/denis" -H  "accept: application/json"

```
 
### Response
```
connection: keep-alive 
 content-type: application/json 
 date: Mon,08 Mar 2021 18:28:03 GMT 
 keep-alive: timeout=60 
 transfer-encoding: chunked 
 vary: Origin,Access-Control-Request-Method,Access-Control-Request-Headers 

{
  "transactions": [
    {
      "transactionLogId": "1",
      "amount": 5000,
      "playerName": "denis",
      "operation": "CREDIT",
      "baseModel": {
        "insertDate": "Mon, 08 Mar 2021 19:17:04 CET"
      }
    },
    {
      "transactionLogId": "2",
      "amount": 5000,
      "playerName": "denis",
      "operation": "DEBIT",
      "baseModel": {
        "insertDate": "Mon, 08 Mar 2021 19:26:11 CET"
      }
    }
  ]
}
```

## Current balance per player.
### Request
```
GET http://localhost:8080//api/transactions/balance/{playerName}

curl -X GET "http://localhost:8080/api/transactions/balance/denis" -H  "accept: application/json"


```
 
### Response
```
connection: keep-alive 
 content-type: application/json 
 date: Mon,08 Mar 2021 18:31:36 GMT 
 keep-alive: timeout=60 
 transfer-encoding: chunked 
 vary: Origin,Access-Control-Request-Method,Access-Control-Request-Headers  

{
  "currentBalance": 0
}
```

## ERRORS
Response errors are returned in a Map with Error code as the key and message error description as the message.
An example:
```
{
  "errorMessages": {
    "ERR_04": "This transaction id is already used!"
  }
}
```

The List of all Error codes and their related messages are:
```
errorMessages:
  - ERR_01,Transaction id field is missing or is not valid (Max length = 10 - pattern =^[a-zA-Z0-9]+$)!
  - ERR_02,The player name is missing or is not valid (Max length = 10 - pattern =^[a-zA-Z0-9]+$)!
  - ERR_03,Amount field is missing or is not valid(<9 digits>.<2digits> expected)!
  - ERR_04,This transaction id is already used!
  - ERR_05,This player has no transaction!
  - ERR_06,Generic error!
  - ERR_07,This transaction id not exists!
  - ERR_08,This amount is greater than current balance!

```

 
## Thanks!
